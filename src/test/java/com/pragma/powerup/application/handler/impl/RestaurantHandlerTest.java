package com.pragma.powerup.application.handler.impl;

import com.pragma.powerup.application.dto.request.RestaurantRequestDto;
import com.pragma.powerup.application.mapper.IRestaurantRequestMapper;
import com.pragma.powerup.domain.api.IRestaurantServicePort;
import com.pragma.powerup.domain.model.RestaurantModel;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class RestaurantHandlerTest {

    @Mock
    private IRestaurantServicePort restaurantServicePort;

    @Mock
    private IRestaurantRequestMapper restaurantRequestMapper;

    @InjectMocks
    private RestaurantHandler restaurantHandler;

    public RestaurantHandlerTest() {
        MockitoAnnotations.initMocks(this);
    }
    @Test
    void saveRestaurantSuccessfully() {
        RestaurantRequestDto restaurantRequestDto = new RestaurantRequestDto();
        RestaurantModel restaurantModel = new RestaurantModel();
        when(restaurantRequestMapper.toRestaurant(restaurantRequestDto)).thenReturn(restaurantModel);

        restaurantHandler.saveRestaurant(restaurantRequestDto);

        verify(restaurantRequestMapper).toRestaurant(restaurantRequestDto);
        verify(restaurantServicePort).saveRestaurant(restaurantModel);
    }
}