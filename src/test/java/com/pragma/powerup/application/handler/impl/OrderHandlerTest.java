package com.pragma.powerup.application.handler.impl;

import com.pragma.powerup.application.dto.request.OrderRequestDto;
import com.pragma.powerup.application.mapper.IDishQuantityRequestMapper;
import com.pragma.powerup.application.mapper.IOrderRequestMapper;
import com.pragma.powerup.domain.api.IOrderServicePort;
import com.pragma.powerup.domain.model.DishQuantityModel;
import com.pragma.powerup.domain.model.OrderModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class OrderHandlerTest {

    @Mock
    private IOrderServicePort orderServicePort;

    @Mock
    private IOrderRequestMapper orderRequestMapper;

    @Mock
    private IDishQuantityRequestMapper dishQuantityRequestMapper;

    @InjectMocks
    private OrderHandler orderHandler;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void saveOrder_ValidOrderRequestDto_CallsServicePortWithMappedData() {
        OrderRequestDto orderRequestDto = new OrderRequestDto();
        OrderModel orderModel = new OrderModel();
        Set<DishQuantityModel> dishQuantityModels = Collections.singleton(new DishQuantityModel());

        when(orderRequestMapper.toOrder(orderRequestDto)).thenReturn(orderModel);
        when(dishQuantityRequestMapper.toDishQuantities(orderRequestDto.getDishQuantities())).thenReturn(dishQuantityModels);

        orderHandler.saveOrder(orderRequestDto, "token");
        verify(orderServicePort).saveOrder(orderModel, dishQuantityModels, "token");
    }
}