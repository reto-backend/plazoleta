package com.pragma.powerup.application.handler.impl;

import com.pragma.powerup.application.dto.request.DishRequestDto;
import com.pragma.powerup.application.mapper.IDishRequestMapper;
import com.pragma.powerup.domain.api.IDishServicePort;
import com.pragma.powerup.domain.exception.FieldNotUpdatableException;
import com.pragma.powerup.domain.model.DishModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DishHandlerTest {

    @Mock
    private IDishServicePort dishServicePort;

    @Mock
    private IDishRequestMapper dishRequestMapper;

    @InjectMocks
    private DishHandler dishHandler;

    @Captor
    private ArgumentCaptor<DishModel> dishModelCaptor;

    private String token;

    public DishHandlerTest() {
        MockitoAnnotations.initMocks(this);
    }

    @BeforeEach
    void setUp() {
        token = "token";
    }

    @Test
    void saveDishSuccessfully() {
        DishRequestDto dishRequestDto = new DishRequestDto();
        DishModel dishModel = new DishModel();
        when(dishRequestMapper.toDish(dishRequestDto)).thenReturn(dishModel);

        dishHandler.saveDish(dishRequestDto, token);

        verify(dishRequestMapper).toDish(dishRequestDto);
        verify(dishServicePort).saveDish(dishModel, token);
    }

    @Test
    void updateDishNameNotNullThrowsFieldNotUpdatableException() {
        DishRequestDto dishRequestDto = new DishRequestDto();
        dishRequestDto.setName("New Name");

        Long id = 1L;

        assertThrows(FieldNotUpdatableException.class, () -> dishHandler.updateDish(dishRequestDto, id, token));
    }

    @Test
    void updateDishCategoryIdNotNullThrowsFieldNotUpdatableException() {
        DishRequestDto dishRequestDto = new DishRequestDto();
        dishRequestDto.setCategoryId(1L);

        Long id = 1L;

        assertThrows(FieldNotUpdatableException.class, () -> dishHandler.updateDish(dishRequestDto, id, token));
    }

    @Test
    void updateDishRestaurantIdNotNullThrowsFieldNotUpdatableException() {
        DishRequestDto dishRequestDto = new DishRequestDto();
        dishRequestDto.setRestaurantId(1L);

        Long id = 1L;

        assertThrows(FieldNotUpdatableException.class, () -> dishHandler.updateDish(dishRequestDto, id, token));
    }

    @Test
    void updateDishUrlImageNotNullThrowsFieldNotUpdatableException() {
        DishRequestDto dishRequestDto = new DishRequestDto();
        dishRequestDto.setUrlImage("New Url Image");

        Long id = 1L;

        assertThrows(FieldNotUpdatableException.class, () -> dishHandler.updateDish(dishRequestDto, id, token));
    }

    @Test
    void updateDishAllFieldsNullSuccessfulUpdate() {
        DishRequestDto dishRequestDto = new DishRequestDto();

        Long id = 1L;

        DishModel dishModel = new DishModel();
        dishModel.setId(id);

        when(dishRequestMapper.toDish(dishRequestDto)).thenReturn(dishModel);

        dishHandler.updateDish(dishRequestDto, id, token);

        verify(dishServicePort, times(1)).updateDish(dishModel, token);
    }

    @Test
    void updateDishDescriptionNotNullSuccessfulUpdate() {
        DishRequestDto dishRequestDto = new DishRequestDto();
        dishRequestDto.setDescription("New Description");

        Long id = 1L;

        DishModel dishModel = new DishModel();
        dishModel.setId(id);

        when(dishRequestMapper.toDish(dishRequestDto)).thenReturn(dishModel);

        dishHandler.updateDish(dishRequestDto, id, token);

        verify(dishServicePort, times(1)).updateDish(dishModel, token);
    }

    @Test
    void updateDishPriceNotNullSuccessfulUpdate() {
        DishRequestDto dishRequestDto = new DishRequestDto();
        dishRequestDto.setPrice(100L);

        Long id = 1L;

        DishModel dishModel = new DishModel();
        dishModel.setId(id);

        when(dishRequestMapper.toDish(dishRequestDto)).thenReturn(dishModel);

        dishHandler.updateDish(dishRequestDto, id, token);

        verify(dishServicePort, times(1)).updateDish(dishModel, token);
    }

    @Test
    void activateDishValidIdActivatesDish() {
        Long dishId = 1L;
        String token = "valid_token";

        dishHandler.activateDish(dishId, token);

        verify(dishServicePort).updateDish(dishModelCaptor.capture(), any(String.class));
        DishModel capturedModel = dishModelCaptor.getValue();

        assertNotNull(capturedModel);
        assertTrue(capturedModel.isActive());
    }

    @Test
    void deactivateDishValidIdDeactivatesDish() {
        Long dishId = 1L;
        String token = "valid_token";

        dishHandler.deactivateDish(dishId, token);

        verify(dishServicePort).updateDish(dishModelCaptor.capture(), any(String.class));
        DishModel capturedModel = dishModelCaptor.getValue();

        assertNotNull(capturedModel);
        assertFalse(capturedModel.isActive());
    }
}