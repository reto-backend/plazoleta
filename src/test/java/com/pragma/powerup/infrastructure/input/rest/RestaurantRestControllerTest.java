package com.pragma.powerup.infrastructure.input.rest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class RestaurantRestControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        // User with id 1 is a propietario and User with id 2 is a cliente
    }

    @Test
    void saveRestaurantSuccessfully() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/restaurant/")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"address\": \"string\",\n" +
                                "  \"propietarioId\": 1,\n" +
                                "  \"phone\": \"32\",\n" +
                                "  \"urlLogo\": \"string\",\n" +
                                "  \"nit\": \"787\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/restaurant/")
                        .content("{\n" +
                                "  \"name\": \"string123\",\n" +
                                "  \"address\": \"string\",\n" +
                                "  \"propietarioId\": 1,\n" +
                                "  \"phone\": \"32\",\n" +
                                "  \"urlLogo\": \"string\",\n" +
                                "  \"nit\": \"787\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void saveRestaurantBlankFieldReturnsBadRequest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/restaurant/")
                        .content("{\n" +
                                "  \"name\": \"\",\n" +
                                "  \"address\": \"string\",\n" +
                                "  \"propietarioId\": 1,\n" +
                                "  \"phone\": \"32\",\n" +
                                "  \"urlLogo\": \"string\",\n" +
                                "  \"nit\": \"787\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/restaurant/")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"address\": \"\",\n" +
                                "  \"propietarioId\": 1,\n" +
                                "  \"phone\": \"32\",\n" +
                                "  \"urlLogo\": \"string\",\n" +
                                "  \"nit\": \"787\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
        // Ojo con el propietarioId
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/restaurant/")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"address\": \"string\",\n" +
                                "  \"propietarioId\": ,\n" +
                                "  \"phone\": \"32\",\n" +
                                "  \"urlLogo\": \"string\",\n" +
                                "  \"nit\": \"787\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/restaurant/")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"address\": \"string\",\n" +
                                "  \"propietarioId\": 1,\n" +
                                "  \"phone\": \"\",\n" +
                                "  \"urlLogo\": \"string\",\n" +
                                "  \"nit\": \"787\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/restaurant/")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"address\": \"string\",\n" +
                                "  \"propietarioId\": 1,\n" +
                                "  \"phone\": \"32\",\n" +
                                "  \"urlLogo\": \"\",\n" +
                                "  \"nit\": \"787\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/restaurant/")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"address\": \"string\",\n" +
                                "  \"propietarioId\": 1,\n" +
                                "  \"phone\": \"32\",\n" +
                                "  \"urlLogo\": \"string\",\n" +
                                "  \"nit\": \"\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void saveRestaurantInvalidUserRoleReturnsBadRequest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/restaurant/")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"address\": \"string\",\n" +
                                "  \"propietarioId\": 2,\n" +
                                "  \"phone\": \"32\",\n" +
                                "  \"urlLogo\": \"string\",\n" +
                                "  \"nit\": \"787\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void saveRestaurantUserNotFoundReturnsNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/restaurant/")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"address\": \"string\",\n" +
                                "  \"propietarioId\": 9999999999999,\n" +
                                "  \"phone\": \"32\",\n" +
                                "  \"urlLogo\": \"string\",\n" +
                                "  \"nit\": \"787\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void saveRestaurantInvalidPhoneReturnsBadRequest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/restaurant/")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"address\": \"string\",\n" +
                                "  \"propietarioId\": 1,\n" +
                                "  \"phone\": \"+1243a\",\n" +
                                "  \"urlLogo\": \"string\",\n" +
                                "  \"nit\": \"787\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/restaurant/")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"address\": \"string\",\n" +
                                "  \"propietarioId\": 1,\n" +
                                "  \"phone\": \"++1243\",\n" +
                                "  \"urlLogo\": \"string\",\n" +
                                "  \"nit\": \"787\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/restaurant/")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"address\": \"string\",\n" +
                                "  \"propietarioId\": 1,\n" +
                                "  \"phone\": \"+1243a\",\n" +
                                "  \"urlLogo\": \"string\",\n" +
                                "  \"nit\": \"787\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void saveRestaurantInvalidNitReturnsBadRequest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/restaurant/")
                        .content("{\n" +
                                "  \"name\": \"string\",\n" +
                                "  \"address\": \"string\",\n" +
                                "  \"propietarioId\": 1,\n" +
                                "  \"phone\": \"9999999999999\",\n" +
                                "  \"urlLogo\": \"string\",\n" +
                                "  \"nit\": \"787a\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void saveRestaurantInvalidNameReturnsBadRequest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/restaurant/")
                        .content("{\n" +
                                "  \"name\": \"1827672\",\n" +
                                "  \"address\": \"string\",\n" +
                                "  \"propietarioId\": 1,\n" +
                                "  \"phone\": \"+1243\",\n" +
                                "  \"urlLogo\": \"string\",\n" +
                                "  \"nit\": \"787\"\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
}