package com.pragma.powerup.infrastructure.input.rest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class DishRestControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUp(){
        // Ensure there is a restaurant and a category with id 1
    }

    @Test
    void saveDishSuccessfully() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/dish/")
                        .content("{\n" +
                                "  \"name\": \"Dishy\",\n" +
                                "  \"categoryId\": 1,\n" +
                                "  \"description\": \"A great dish\",\n" +
                                "  \"price\": 4000,\n" +
                                "  \"restaurantId\": 1,\n" +
                                "  \"urlImage\": \"string\"\n" +
                                "}")
                        .header("Authorization", "Bearer eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJwcm9waWV0YXJpbzFAcHJhZ21hLmNvbS5jbyIsImV4cCI6MTcwNjEyNzgxNiwicm9sZSI6IlJPTEVfUFJPUElFVEFSSU8iLCJuYW1lIjoic3RyaW5nIiwiaWQiOjF9.vi_wUKC1kueumigu6UVC639ww0KsrvFxN93h_hB_pA8tlFM4O81MLZduRJwBbHfX")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void saveDishBlankFieldReturnsBadRequest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/dish/")
                        .content("{\n" +
                                "  \"name\": \"\",\n" +
                                "  \"categoryId\": 1,\n" +
                                "  \"description\": \"A great dish\",\n" +
                                "  \"price\": 4000,\n" +
                                "  \"restaurantId\": 1,\n" +
                                "  \"urlImage\": \"string\"\n" +
                                "}")
                        .header("Authorization", "Bearer eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJwcm9waWV0YXJpbzFAcHJhZ21hLmNvbS5jbyIsImV4cCI6MTcwNjEyNzgxNiwicm9sZSI6IlJPTEVfUFJPUElFVEFSSU8iLCJuYW1lIjoic3RyaW5nIiwiaWQiOjF9.vi_wUKC1kueumigu6UVC639ww0KsrvFxN93h_hB_pA8tlFM4O81MLZduRJwBbHfX")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/dish/")
                        .content("{\n" +
                                "  \"name\": \"Dishy\",\n" +
                                "  \"categoryId\": ,\n" +
                                "  \"description\": \"A great dish\",\n" +
                                "  \"price\": 4000,\n" +
                                "  \"restaurantId\": 1,\n" +
                                "  \"urlImage\": \"string\"\n" +
                                "}")
                        .header("Authorization", "Bearer eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJwcm9waWV0YXJpbzFAcHJhZ21hLmNvbS5jbyIsImV4cCI6MTcwNjEyNzgxNiwicm9sZSI6IlJPTEVfUFJPUElFVEFSSU8iLCJuYW1lIjoic3RyaW5nIiwiaWQiOjF9.vi_wUKC1kueumigu6UVC639ww0KsrvFxN93h_hB_pA8tlFM4O81MLZduRJwBbHfX")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/dish/")
                        .content("{\n" +
                                "  \"name\": \"Dishy\",\n" +
                                "  \"categoryId\": 1,\n" +
                                "  \"description\": \"\",\n" +
                                "  \"price\": 4000,\n" +
                                "  \"restaurantId\": 1,\n" +
                                "  \"urlImage\": \"string\"\n" +
                                "}")
                        .header("Authorization", "Bearer eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJwcm9waWV0YXJpbzFAcHJhZ21hLmNvbS5jbyIsImV4cCI6MTcwNjEyNzgxNiwicm9sZSI6IlJPTEVfUFJPUElFVEFSSU8iLCJuYW1lIjoic3RyaW5nIiwiaWQiOjF9.vi_wUKC1kueumigu6UVC639ww0KsrvFxN93h_hB_pA8tlFM4O81MLZduRJwBbHfX")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/dish/")
                        .content("{\n" +
                                "  \"name\": \"Dishy\",\n" +
                                "  \"categoryId\": 1,\n" +
                                "  \"description\": \"A great dish\",\n" +
                                "  \"price\": ,\n" +
                                "  \"restaurantId\": 1,\n" +
                                "  \"urlImage\": \"string\"\n" +
                                "}")
                        .header("Authorization", "Bearer eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJwcm9waWV0YXJpbzFAcHJhZ21hLmNvbS5jbyIsImV4cCI6MTcwNjEyNzgxNiwicm9sZSI6IlJPTEVfUFJPUElFVEFSSU8iLCJuYW1lIjoic3RyaW5nIiwiaWQiOjF9.vi_wUKC1kueumigu6UVC639ww0KsrvFxN93h_hB_pA8tlFM4O81MLZduRJwBbHfX")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/dish/")
                        .content("{\n" +
                                "  \"name\": \"Dishy\",\n" +
                                "  \"categoryId\": 1,\n" +
                                "  \"description\": \"A great dish\",\n" +
                                "  \"price\": 4000,\n" +
                                "  \"restaurantId\": ,\n" +
                                "  \"urlImage\": \"string\"\n" +
                                "}")
                        .header("Authorization", "Bearer eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJwcm9waWV0YXJpbzFAcHJhZ21hLmNvbS5jbyIsImV4cCI6MTcwNjEyNzgxNiwicm9sZSI6IlJPTEVfUFJPUElFVEFSSU8iLCJuYW1lIjoic3RyaW5nIiwiaWQiOjF9.vi_wUKC1kueumigu6UVC639ww0KsrvFxN93h_hB_pA8tlFM4O81MLZduRJwBbHfX")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/dish/")
                        .content("{\n" +
                                "  \"name\": \"Dishy\",\n" +
                                "  \"categoryId\": 1,\n" +
                                "  \"description\": \"A great dish\",\n" +
                                "  \"price\": 4000,\n" +
                                "  \"restaurantId\": 1,\n" +
                                "  \"urlImage\": \"\"\n" +
                                "}")
                        .header("Authorization", "Bearer eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJwcm9waWV0YXJpbzFAcHJhZ21hLmNvbS5jbyIsImV4cCI6MTcwNjEyNzgxNiwicm9sZSI6IlJPTEVfUFJPUElFVEFSSU8iLCJuYW1lIjoic3RyaW5nIiwiaWQiOjF9.vi_wUKC1kueumigu6UVC639ww0KsrvFxN93h_hB_pA8tlFM4O81MLZduRJwBbHfX")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void saveDishRestaurantNotFoundReturnsNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/dish/")
                        .content("{\n" +
                                "  \"name\": \"Dishy\",\n" +
                                "  \"categoryId\": 1,\n" +
                                "  \"description\": \"A great dish\",\n" +
                                "  \"price\": 4000,\n" +
                                "  \"restaurantId\": 999999,\n" +
                                "  \"urlImage\": \"string\"\n" +
                                "}")
                        .header("Authorization", "Bearer eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJwcm9waWV0YXJpbzFAcHJhZ21hLmNvbS5jbyIsImV4cCI6MTcwNjEyNzgxNiwicm9sZSI6IlJPTEVfUFJPUElFVEFSSU8iLCJuYW1lIjoic3RyaW5nIiwiaWQiOjF9.vi_wUKC1kueumigu6UVC639ww0KsrvFxN93h_hB_pA8tlFM4O81MLZduRJwBbHfX")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void updateDishInvalidPriceReturnsBadRequest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/dish/1")
                        .content("{\n" +
                                "  \"price\": -4000\n" +
                                "}")
                        .header("Authorization", "Bearer eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJwcm9waWV0YXJpbzFAcHJhZ21hLmNvbS5jbyIsImV4cCI6MTcwNjEyNzgxNiwicm9sZSI6IlJPTEVfUFJPUElFVEFSSU8iLCJuYW1lIjoic3RyaW5nIiwiaWQiOjF9.vi_wUKC1kueumigu6UVC639ww0KsrvFxN93h_hB_pA8tlFM4O81MLZduRJwBbHfX")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc.perform(MockMvcRequestBuilders.patch("/api/v1/dish/1")
                        .content("{\n" +
                                "  \"price\": 0\n" +
                                "}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void saveDishCategoryNotFoundReturnsNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/dish/")
                        .content("{\n" +
                                "  \"name\": \"Dishy\",\n" +
                                "  \"categoryId\": 999999,\n" +
                                "  \"description\": \"A great dish\",\n" +
                                "  \"price\": 4000,\n" +
                                "  \"restaurantId\": 1,\n" +
                                "  \"urlImage\": \"string\"\n" +
                                "}")
                        .header("Authorization", "Bearer eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJwcm9waWV0YXJpbzFAcHJhZ21hLmNvbS5jbyIsImV4cCI6MTcwNjEyNzgxNiwicm9sZSI6IlJPTEVfUFJPUElFVEFSSU8iLCJuYW1lIjoic3RyaW5nIiwiaWQiOjF9.vi_wUKC1kueumigu6UVC639ww0KsrvFxN93h_hB_pA8tlFM4O81MLZduRJwBbHfX")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void saveDishInvalidUserRoleReturnsForbidden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/dish/")
                        .content("{\n" +
                                "  \"name\": \"Dishy\",\n" +
                                "  \"categoryId\": 1,\n" +
                                "  \"description\": \"A great dish\",\n" +
                                "  \"price\": 4000,\n" +
                                "  \"restaurantId\": 1,\n" +
                                "  \"urlImage\": \"string\"\n" +
                                "}")
                        .header("Authorization", "eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJ2YWxzNG9vQGdtYWlsLmNvbSIsImV4cCI6MTcwNjEyODM3MSwicm9sZSI6IlJPTEVfQURNSU5JU1RSQURPUiIsIm5hbWUiOiJFbWlsaW8iLCJpZCI6MjF9.ssbYE-rkOk9I_Jre7Z-jk9bUicv7ppJvTQYQAJ9BCTEqxnNSOhJ4mE_x8VHqabxb")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    void saveDishInvalidPropietarioIdReturnsForbidden() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/dish/")
                        .content("{\n" +
                                "  \"name\": \"Dishy\",\n" +
                                "  \"categoryId\": 1,\n" +
                                "  \"description\": \"A great dish\",\n" +
                                "  \"price\": 4000,\n" +
                                "  \"restaurantId\": 1,\n" +
                                "  \"urlImage\": \"string\"\n" +
                                "}")
                        .header("Authorization", "eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJwcm9waWV0YXJpb0BwcmFnbWEuY29tLmNvIiwiZXhwIjoxNzA2MTI4NTc0LCJyb2xlIjoiUk9MRV9QUk9QSUVUQVJJTyIsIm5hbWUiOiJwbGF6b2xldGEiLCJpZCI6MjZ9.vxi0stLdpZjbpBTzbIeLYrQgvjiKG7h7627-M2x6ibFK32SU_NJCY3R4eoAeY0nz")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
        }
    @Test
    void listDishesByRestaurantSuccessfully() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/dish/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void listDishesByRestaurantAndCategorySuccessfully() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/dish/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}