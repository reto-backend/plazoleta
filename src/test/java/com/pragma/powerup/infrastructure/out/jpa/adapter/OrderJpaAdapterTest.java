package com.pragma.powerup.infrastructure.out.jpa.adapter;

import com.pragma.powerup.application.dto.response.EmpleadoResponseDto;
import com.pragma.powerup.domain.model.DishModel;
import com.pragma.powerup.domain.model.DishQuantityModel;
import com.pragma.powerup.domain.model.OrderModel;
import com.pragma.powerup.domain.model.RestaurantModel;
import com.pragma.powerup.infrastructure.exception.InvalidDishException;
import com.pragma.powerup.infrastructure.exception.InvalidEmployeeException;
import com.pragma.powerup.infrastructure.exception.InvalidRequestException;
import com.pragma.powerup.infrastructure.exception.NoDataFoundException;
import com.pragma.powerup.infrastructure.out.jpa.entity.DishEntity;
import com.pragma.powerup.infrastructure.out.jpa.entity.OrderEntity;
import com.pragma.powerup.infrastructure.out.jpa.entity.RestaurantEntity;
import com.pragma.powerup.infrastructure.out.jpa.feign.client.IMessageFeignClient;
import com.pragma.powerup.infrastructure.out.jpa.feign.client.IUserFeignClient;
import com.pragma.powerup.infrastructure.out.jpa.mapper.IDishQuantityEntityMapper;
import com.pragma.powerup.infrastructure.out.jpa.mapper.IOrderEntityMapper;
import com.pragma.powerup.infrastructure.out.jpa.repository.IDishQuantityRepository;
import com.pragma.powerup.infrastructure.out.jpa.repository.IDishRepository;
import com.pragma.powerup.infrastructure.out.jpa.repository.IOrderRepository;
import com.pragma.powerup.infrastructure.out.jpa.repository.IRestaurantRepository;
import com.pragma.powerup.infrastructure.security.TokenUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

class OrderJpaAdapterTest {

    @Mock
    private IOrderRepository orderRepository;

    @Mock
    private IOrderEntityMapper orderEntityMapper;

    @Mock
    private IDishQuantityRepository dishQuantityRepository;

    @Mock
    private IDishQuantityEntityMapper dishQuantityEntityMapper;

    @Mock
    private IRestaurantRepository restaurantRepository;

    @Mock
    private IDishRepository dishRepository;

    @Mock
    private IUserFeignClient userFeignClient;

    @Mock
    private IMessageFeignClient messageFeignClient;

    @InjectMocks
    private OrderJpaAdapter orderJpaAdapter;

    public OrderJpaAdapterTest() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void saveOrderNoRestaurantFoundThrowsNoDataFoundException() {
        Long restaurantId = 1L;
        OrderModel orderModel = new OrderModel();
        Set<DishQuantityModel> dishQuantityModels = Collections.singleton(new DishQuantityModel());

        orderModel.setRestaurant(new RestaurantModel());
        orderModel.getRestaurant().setId(restaurantId);

        when(restaurantRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(NoDataFoundException.class, () ->
                orderJpaAdapter.saveOrder(orderModel, dishQuantityModels, "valid_token"));
    }

    @Test
    void saveOrderDishNotFoundThrowsNoDataFoundException() {
        Long restaurantId = 1L;
        OrderModel orderModel = new OrderModel();
        DishQuantityModel dishQuantityModel = new DishQuantityModel();
        dishQuantityModel.setDish(new DishModel());
        dishQuantityModel.getDish().setId(999L);
        Set<DishQuantityModel> dishQuantityModels = Collections.singleton(dishQuantityModel);

        orderModel.setRestaurant(new RestaurantModel());
        orderModel.getRestaurant().setId(restaurantId);

        when(restaurantRepository.findById(anyLong())).thenReturn(Optional.of(new RestaurantEntity()));
        when(dishRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(NoDataFoundException.class, () ->
                orderJpaAdapter.saveOrder(orderModel, dishQuantityModels, "valid_token"));
    }

    @Test
    void saveOrderInvalidDishThrowsInvalidDishException() {
        Long restaurantId = 2L;
        OrderModel orderModel = new OrderModel();
        DishQuantityModel dishQuantityModel = new DishQuantityModel();
        dishQuantityModel.setDish(new DishModel());
        dishQuantityModel.getDish().setId(1L);
        Set<DishQuantityModel> dishQuantityModels = Collections.singleton(dishQuantityModel);

        orderModel.setRestaurant(new RestaurantModel());
        orderModel.getRestaurant().setId(restaurantId);

        DishEntity returnDishEntity = new DishEntity();
        RestaurantEntity returnRestaurantEntity = new RestaurantEntity();
        returnRestaurantEntity.setId(1L);
        returnDishEntity.setRestaurant(returnRestaurantEntity);

        when(restaurantRepository.findById(anyLong())).thenReturn(Optional.of(new RestaurantEntity()));
        when(dishRepository.findById(anyLong())).thenReturn(Optional.of(returnDishEntity));

        assertThrows(InvalidDishException.class, () ->
                orderJpaAdapter.saveOrder(orderModel, dishQuantityModels, "valid_token"));
    }

    @Test
    void assignEmployee_OrderNotFound_ThrowsNoDataFoundException() {
        Long orderId = 1L;
        String token = "valid_token";

        when(orderRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(NoDataFoundException.class, () -> orderJpaAdapter.assignEmployee(orderId, token));

        verify(orderRepository, times(1)).findById(orderId);
        verifyNoMoreInteractions(orderRepository, orderEntityMapper, userFeignClient);
    }

    @Test
    void assignEmployee_InvalidEmployee_ThrowsInvalidEmployeeException() {
        Long orderId = 1L;
        String token = "eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJlbXBsZWFkb0BwcmFnbWEuY29tLmNvIiwiZXhwIjoxNzA2NjU0NjMzLCJyb2xlIjoiUk9MRV9FTVBMRUFETyIsIm5hbWUiOiJlbXBsZWFkbyIsImlkIjo2OH0.vAPZEc_HzpHygjVIGUEEj5SfIJGLGJvxFnBzfRidux-SonXftlfS9Rp6IQOJkX9F";

        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setRestaurant(new RestaurantEntity());
        orderEntity.getRestaurant().setId(1L);
        when(orderRepository.findById(anyLong())).thenReturn(Optional.of(orderEntity));
        EmpleadoResponseDto empleadoResponseDto = new EmpleadoResponseDto();
        empleadoResponseDto.setRestaurantId(2L);
        when(userFeignClient.getEmpleado(anyLong())).thenReturn(empleadoResponseDto);

        assertThrows(InvalidEmployeeException.class, () -> orderJpaAdapter.assignEmployee(orderId, token));

        verify(orderRepository, times(1)).findById(orderId);
        verify(userFeignClient, times(1)).getEmpleado(TokenUtils.getUserId(token));
        verifyNoMoreInteractions(orderRepository, orderEntityMapper, userFeignClient);
    }

    @Test
    void assignEmployeeSuccessfully() {
        Long orderId = 1L;
        String token = "eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJlbXBsZWFkb0BwcmFnbWEuY29tLmNvIiwiZXhwIjoxNzA2NjU0NjMzLCJyb2xlIjoiUk9MRV9FTVBMRUFETyIsIm5hbWUiOiJlbXBsZWFkbyIsImlkIjo2OH0.vAPZEc_HzpHygjVIGUEEj5SfIJGLGJvxFnBzfRidux-SonXftlfS9Rp6IQOJkX9F";

        OrderEntity orderEntity = new OrderEntity();
        RestaurantEntity restaurantEntity = new RestaurantEntity();
        restaurantEntity.setId(1L);
        orderEntity.setRestaurant(restaurantEntity);

        when(orderRepository.findById(anyLong())).thenReturn(Optional.of(orderEntity));

        EmpleadoResponseDto empleadoResponseDto = new EmpleadoResponseDto();
        empleadoResponseDto.setRestaurantId(1L);
        when(userFeignClient.getEmpleado(anyLong())).thenReturn(empleadoResponseDto);

        OrderModel result = orderJpaAdapter.assignEmployee(orderId, token);

        verify(orderRepository, times(1)).findById(orderId);
        verify(userFeignClient, times(1)).getEmpleado(anyLong());
        verify(orderRepository, times(1)).save(orderEntity);
        verifyNoMoreInteractions(orderRepository, userFeignClient);
    }

    @Test
    void orderReady_OrderNotFound_ThrowsNoDataFoundException() {
        Long orderId = 1L;
        String token = "valid_token";

        when(orderRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(NoDataFoundException.class, () -> orderJpaAdapter.orderReady(orderId, token));

        verify(orderRepository, times(1)).findById(orderId);
        verifyNoMoreInteractions(orderRepository, orderEntityMapper, userFeignClient);
    }

    @Test
    void orderReady_InvalidEmployee_ThrowsInvalidEmployeeException() {
        Long orderId = 1L;
        String token = "eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJlbXBsZWFkb0BwcmFnbWEuY29tLmNvIiwiZXhwIjoxNzA2NjU0NjMzLCJyb2xlIjoiUk9MRV9FTVBMRUFETyIsIm5hbWUiOiJlbXBsZWFkbyIsImlkIjo2OH0.vAPZEc_HzpHygjVIGUEEj5SfIJGLGJvxFnBzfRidux-SonXftlfS9Rp6IQOJkX9F";

        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setChefId(999L);
        when(orderRepository.findById(anyLong())).thenReturn(Optional.of(orderEntity));

        assertThrows(InvalidEmployeeException.class, () -> orderJpaAdapter.orderReady(orderId, token));

        verify(orderRepository, times(1)).findById(orderId);
        verifyNoMoreInteractions(orderRepository, orderEntityMapper, userFeignClient, messageFeignClient);
    }

    @Test
    void deliverOrder_OrderNotFound_ThrowsNoDataFoundException() {
        Long orderId = 1L;
        String token = "valid_token";
        String securityPin = "1234";

        when(orderRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(NoDataFoundException.class, () -> orderJpaAdapter.deliverOrder(orderId, token, securityPin));

        verify(orderRepository, times(1)).findById(orderId);
        verifyNoMoreInteractions(orderRepository, orderEntityMapper, userFeignClient);
    }

    @Test
    void deliverOrder_InvalidEmployee_ThrowsInvalidEmployeeException() {
        Long orderId = 1L;
        String token = "eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJlbXBsZWFkb0BwcmFnbWEuY29tLmNvIiwiZXhwIjoxNzA2NjU0NjMzLCJyb2xlIjoiUk9MRV9FTVBMRUFETyIsIm5hbWUiOiJlbXBsZWFkbyIsImlkIjo2OH0.vAPZEc_HzpHygjVIGUEEj5SfIJGLGJvxFnBzfRidux-SonXftlfS9Rp6IQOJkX9F";
        String securityPin = "1234";

        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setChefId(999L);
        when(orderRepository.findById(anyLong())).thenReturn(Optional.of(orderEntity));

        assertThrows(InvalidEmployeeException.class, () -> orderJpaAdapter.deliverOrder(orderId, token, securityPin));

        verify(orderRepository, times(1)).findById(orderId);
        verifyNoMoreInteractions(orderRepository, orderEntityMapper, userFeignClient, messageFeignClient);
    }

    @Test
    void cancelOrder_OrderNotFound_ThrowsNoDataFoundException() {
        Long orderId = 1L;
        String token = "valid_token";

        when(orderRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(NoDataFoundException.class, () -> orderJpaAdapter.cancelOrder(orderId, token));

        verify(orderRepository, times(1)).findById(orderId);
        verifyNoMoreInteractions(orderRepository, orderEntityMapper, userFeignClient);
    }

    @Test
    void cancelOrder_InvalidClient_ThrowsInvalidRequestException() {
        Long orderId = 1L;
        String token = "eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJlbXBsZWFkb0BwcmFnbWEuY29tLmNvIiwiZXhwIjoxNzA2NjU0NjMzLCJyb2xlIjoiUk9MRV9FTVBMRUFETyIsIm5hbWUiOiJlbXBsZWFkbyIsImlkIjo2OH0.vAPZEc_HzpHygjVIGUEEj5SfIJGLGJvxFnBzfRidux-SonXftlfS9Rp6IQOJkX9F";

        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setClienteId(999L);
        when(orderRepository.findById(anyLong())).thenReturn(Optional.of(orderEntity));

        assertThrows(InvalidRequestException.class, () -> orderJpaAdapter.cancelOrder(orderId, token));

        verify(orderRepository, times(1)).findById(orderId);
        verifyNoMoreInteractions(orderRepository, orderEntityMapper, userFeignClient, messageFeignClient);
    }
}