package com.pragma.powerup.infrastructure.out.jpa.adapter;

import com.pragma.powerup.domain.model.CategoryModel;
import com.pragma.powerup.domain.model.DishModel;
import com.pragma.powerup.domain.model.RestaurantModel;
import com.pragma.powerup.infrastructure.exception.DishNotFoundException;
import com.pragma.powerup.infrastructure.exception.RestaurantNotFoundException;
import com.pragma.powerup.infrastructure.out.jpa.entity.DishEntity;
import com.pragma.powerup.infrastructure.out.jpa.mapper.IDishEntityMapper;
import com.pragma.powerup.infrastructure.out.jpa.repository.ICategoryRepository;
import com.pragma.powerup.infrastructure.out.jpa.repository.IDishRepository;
import com.pragma.powerup.infrastructure.out.jpa.repository.IRestaurantRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

class DishJpaAdapterTest {

    @Mock
    private IDishRepository dishRepository;

    @Mock
    private IDishEntityMapper dishEntityMapper;

    @Mock
    private IRestaurantRepository restaurantRepository;

    @Mock
    private ICategoryRepository categoryRepository;

    @InjectMocks
    private DishJpaAdapter dishJpaAdapter;

    private String token;

    public DishJpaAdapterTest() {
        MockitoAnnotations.initMocks(this);
    }

    @BeforeEach
    void setUp() {
        token = "token";
    }

    //Only testing failures because of the need of mocking a static method
    @Test
    void saveDishRestaurantNotFoundThrowsRestaurantNotFoundException() {
        RestaurantModel restaurantModel = new RestaurantModel();
        CategoryModel categoryModel = new CategoryModel();
        DishModel dishModel = new DishModel();
        dishModel.setRestaurant(restaurantModel);
        dishModel.setCategory(categoryModel);

        when(restaurantRepository.findById(any())).thenReturn(Optional.empty());

        assertThrows(RestaurantNotFoundException.class, () -> dishJpaAdapter.saveDish(dishModel, "token"));
    }

    @Test
    void updateDishNonExistingDishThrowsDishNotFoundException() {
        DishModel dishModel = new DishModel();
        dishModel.setId(99999L);

        Optional<DishEntity> optionalDishEntity = Optional.empty();
        when(dishRepository.findById(anyLong())).thenReturn(optionalDishEntity);

        assertThrows(DishNotFoundException.class, () -> dishJpaAdapter.updateDish(dishModel, token));
    }
}