package com.pragma.powerup.infrastructure.out.jpa.adapter;

import com.pragma.powerup.application.dto.response.RoleResponseDto;
import com.pragma.powerup.application.dto.response.UserResponseDto;
import com.pragma.powerup.domain.model.RestaurantModel;
import com.pragma.powerup.domain.shared.RoleEnum;
import com.pragma.powerup.infrastructure.exception.InvalidUserRoleException;
import com.pragma.powerup.infrastructure.exception.UserNotFoundException;
import com.pragma.powerup.infrastructure.out.jpa.entity.RestaurantEntity;
import com.pragma.powerup.infrastructure.out.jpa.feign.client.IUserFeignClient;
import com.pragma.powerup.infrastructure.out.jpa.mapper.IRestaurantEntityMapper;
import com.pragma.powerup.infrastructure.out.jpa.repository.IRestaurantRepository;
import feign.FeignException;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class RestaurantJpaAdapterTest {

    @Mock
    private IRestaurantRepository restaurantRepository;

    @Mock
    private IRestaurantEntityMapper restaurantEntityMapper;

    @Mock
    private IUserFeignClient userFeignClient;

    @InjectMocks
    private RestaurantJpaAdapter restaurantJpaAdapter;

    public RestaurantJpaAdapterTest() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void saveRestaurantValidPropietarioSavesRestaurant() {
        UserResponseDto userResponseDto = new UserResponseDto();
        RoleResponseDto roleResponseDto = new RoleResponseDto();
        roleResponseDto.setId(RoleEnum.PROPIETARIO.getValue());
        userResponseDto.setRole(roleResponseDto);

        when(userFeignClient.getUser(any())).thenReturn(userResponseDto);

        RestaurantModel restaurantModel = new RestaurantModel();
        RestaurantEntity restaurantEntity = new RestaurantEntity();
        when(restaurantEntityMapper.toEntity(any())).thenReturn(restaurantEntity);
        when(restaurantRepository.save(any())).thenReturn(restaurantEntity);
        when(restaurantEntityMapper.toRestaurantModel(any())).thenReturn(restaurantModel);

        RestaurantModel savedRestaurant = restaurantJpaAdapter.saveRestaurant(restaurantModel);

        verify(userFeignClient, times(1)).getUser(any());
        verify(restaurantEntityMapper, times(1)).toEntity(any());
        verify(restaurantRepository, times(1)).save(any());
        verify(restaurantEntityMapper, times(1)).toRestaurantModel(any());
        assertNotNull(savedRestaurant);
    }

    @Test
    void saveRestaurantInvalidUserRoleThrowsInvalidUserRoleException() {
        UserResponseDto userResponseDto = new UserResponseDto();
        RoleResponseDto roleResponseDto = new RoleResponseDto();
        roleResponseDto.setId(RoleEnum.EMPLEADO.getValue());
        userResponseDto.setRole(roleResponseDto);

        when(userFeignClient.getUser(any())).thenReturn(userResponseDto);

        RestaurantModel restaurantModel = new RestaurantModel();
        RestaurantEntity restaurantEntity = new RestaurantEntity();
        when(restaurantEntityMapper.toEntity(any())).thenReturn(restaurantEntity);
        when(restaurantRepository.save(any())).thenReturn(restaurantEntity);
        when(restaurantEntityMapper.toRestaurantModel(any())).thenReturn(restaurantModel);

        assertThrows(InvalidUserRoleException.class, () -> restaurantJpaAdapter.saveRestaurant(restaurantModel));

        verify(userFeignClient, times(1)).getUser(any());
        verify(restaurantEntityMapper, never()).toEntity(any());
        verify(restaurantRepository, never()).save(any());
        verify(restaurantEntityMapper, never()).toRestaurantModel(any());
    }

    @Test
    void saveRestaurantUserNotFound() {
        when(userFeignClient.getUser(anyLong())).thenThrow(FeignException.NotFound.class);

        RestaurantModel restaurantModel = new RestaurantModel();
        restaurantModel.setPropietarioId(999999L);

        assertThrows(UserNotFoundException.class, () -> restaurantJpaAdapter.saveRestaurant(restaurantModel));
    }
}