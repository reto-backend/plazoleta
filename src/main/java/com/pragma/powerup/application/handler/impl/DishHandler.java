package com.pragma.powerup.application.handler.impl;

import com.pragma.powerup.application.dto.request.DishRequestDto;
import com.pragma.powerup.application.handler.IDishHandler;
import com.pragma.powerup.application.mapper.IDishRequestMapper;
import com.pragma.powerup.domain.api.IDishServicePort;
import com.pragma.powerup.domain.exception.FieldNotUpdatableException;
import com.pragma.powerup.domain.model.CategoryModel;
import com.pragma.powerup.domain.model.DishModel;
import com.pragma.powerup.domain.model.RestaurantModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class DishHandler implements IDishHandler {
    private final IDishServicePort dishServicePort;
    private final IDishRequestMapper dishRequestMapper;

    @Override
    public void saveDish(DishRequestDto dishRequestDto, String token) {
        DishModel dishModel = dishRequestMapper.toDish(dishRequestDto);
        RestaurantModel restaurantModel = new RestaurantModel(dishRequestDto.getRestaurantId());
        CategoryModel categoryModel = new CategoryModel(dishRequestDto.getCategoryId());
        dishModel.setRestaurant(restaurantModel);
        dishModel.setCategory(categoryModel);
        dishServicePort.saveDish(dishModel, token);
    }

    @Override
    public void updateDish(DishRequestDto dishRequestDto, Long id, String token) {
        if (dishRequestDto.getName() != null) {
            throw new FieldNotUpdatableException("Name is not updatable");
        }
        if (dishRequestDto.getCategoryId() != null) {
            throw new FieldNotUpdatableException("Category is not updatable");
        }
        if (dishRequestDto.getRestaurantId() != null) {
            throw new FieldNotUpdatableException("Restaurant is not updatable");
        }
        if (dishRequestDto.getUrlImage() != null) {
            throw new FieldNotUpdatableException("UrlImage is not updatable");
        }
        DishModel dishModel = dishRequestMapper.toDish(dishRequestDto);
        dishModel.setId(id);
        dishServicePort.updateDish(dishModel, token);
    }

    @Override
    public void activateDish(Long id, String token) {
        DishModel dishModel = new DishModel();
        dishModel.setId(id);
        dishModel.setActive(true);
        dishServicePort.updateDish(dishModel, token);
    }

    @Override
    public void deactivateDish(Long id, String token) {
        DishModel dishModel = new DishModel();
        dishModel.setId(id);
        dishModel.setActive(false);
        dishServicePort.updateDish(dishModel, token);
    }
}
