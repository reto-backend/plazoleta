package com.pragma.powerup.application.handler;

import com.pragma.powerup.application.dto.request.OrderRequestDto;

public interface IOrderHandler {
    void saveOrder(OrderRequestDto orderRequestDto, String token);

    void assignEmployee(Long orderId, String token);

    void orderReady(Long orderId, String token);

    void deliverOrder(Long orderId, String securityPin, String token);

    void cancelOrder(Long orderId, String token);
}
