package com.pragma.powerup.application.handler.impl;

import com.pragma.powerup.application.dto.request.OrderRequestDto;
import com.pragma.powerup.application.handler.IOrderHandler;
import com.pragma.powerup.application.mapper.IDishQuantityRequestMapper;
import com.pragma.powerup.application.mapper.IOrderRequestMapper;
import com.pragma.powerup.domain.api.IOrderServicePort;
import com.pragma.powerup.domain.model.DishQuantityModel;
import com.pragma.powerup.domain.model.OrderModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Transactional
public class OrderHandler implements IOrderHandler {
    private final IOrderServicePort orderServicePort;
    private final IOrderRequestMapper orderRequestMapper;
    private final IDishQuantityRequestMapper dishQuantityRequestMapper;

    @Override
    public void saveOrder(OrderRequestDto orderRequestDto, String token) {
        OrderModel orderModel = orderRequestMapper.toOrder(orderRequestDto);
        orderModel.setDate(new Date());
        Set<DishQuantityModel> dishQuantityModels = dishQuantityRequestMapper.
                toDishQuantities(orderRequestDto.getDishQuantities());
        orderServicePort.saveOrder(orderModel, dishQuantityModels, token);
    }

    @Override
    public void assignEmployee(Long orderId, String token) {
        orderServicePort.assignEmployee(orderId, token);
    }

    @Override
    public void orderReady(Long orderId, String token) {
        orderServicePort.orderReady(orderId, token);
    }

    @Override
    public void deliverOrder(Long orderId, String securityPin, String token) {
        orderServicePort.deliverOrder(orderId, securityPin, token);
    }

    @Override
    public void cancelOrder(Long orderId, String token) {
        orderServicePort.cancelOrder(orderId, token);
    }
}
