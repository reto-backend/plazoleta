package com.pragma.powerup.application.handler;

import com.pragma.powerup.application.dto.request.DishRequestDto;

public interface IDishHandler {
    void saveDish(DishRequestDto dishRequestDto, String token);

    void updateDish(DishRequestDto dishRequestDto, Long id, String token);

    void activateDish(Long id, String token);

    void deactivateDish(Long id, String token);
}
