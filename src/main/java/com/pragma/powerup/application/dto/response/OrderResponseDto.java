package com.pragma.powerup.application.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Set;

@Getter
@Setter
public class OrderResponseDto {
    private Long id;
    private Long clienteId;
    private Date date;
    private String status;
    private Long chefId;
    private Set<DishQuantityResponseDto> dishQuantities;
}
