package com.pragma.powerup.application.dto.request;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class OrderRequestDto {
    private Long restaurantId;
    private Set<DishQuantityRequestDto> dishQuantities;
}
