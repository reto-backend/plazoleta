package com.pragma.powerup.application.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DishQuantityRequestDto {
    private Long dishId;
    private Long quantity;
}
