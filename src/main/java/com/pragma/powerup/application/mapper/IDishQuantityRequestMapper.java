package com.pragma.powerup.application.mapper;

import com.pragma.powerup.application.dto.request.DishQuantityRequestDto;
import com.pragma.powerup.domain.model.DishModel;
import com.pragma.powerup.domain.model.DishQuantityModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Set;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE,
        unmappedSourcePolicy = org.mapstruct.ReportingPolicy.IGNORE)
public interface IDishQuantityRequestMapper {

    Set<DishQuantityModel> toDishQuantities(Set<DishQuantityRequestDto> dishQuantitiesDto);

    @Mapping(target = "dish", source = "dishId")
    DishQuantityModel toDishQuantity(DishQuantityRequestDto dishQuantityDto);

    default DishModel mapIdToDish(Long dishId) {
        DishModel dishModel = new DishModel();
        dishModel.setId(dishId);
        return dishModel;
    }
}
