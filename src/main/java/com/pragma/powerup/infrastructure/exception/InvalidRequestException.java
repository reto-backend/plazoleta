package com.pragma.powerup.infrastructure.exception;

public class InvalidRequestException extends RuntimeException{

        public InvalidRequestException(String message) {
            super(message);
        }
}
