package com.pragma.powerup.infrastructure.exception;

public class InvalidUserRoleException extends RuntimeException {
    public InvalidUserRoleException() {
        super();
    }
}
