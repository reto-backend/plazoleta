package com.pragma.powerup.infrastructure.exception;

public class InvalidPropietarioException extends RuntimeException{
    public InvalidPropietarioException(String message) {
            super(message);
        }
}

