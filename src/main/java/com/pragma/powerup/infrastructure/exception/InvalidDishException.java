package com.pragma.powerup.infrastructure.exception;

public class InvalidDishException extends RuntimeException{
    public InvalidDishException(String message) {
        super(message);
    }
}
