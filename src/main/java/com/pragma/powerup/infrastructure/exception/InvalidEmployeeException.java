package com.pragma.powerup.infrastructure.exception;

public class InvalidEmployeeException extends RuntimeException{
    public InvalidEmployeeException(String message) {
        super(message);
    }
}
