package com.pragma.powerup.infrastructure.exceptionhandler;

import com.pragma.powerup.domain.exception.FieldNotUpdatableException;
import com.pragma.powerup.infrastructure.exception.*;
import feign.FeignException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.Collections;
import java.util.Map;

@ControllerAdvice
public class ControllerAdvisor {

    private static final String MESSAGE = "message";

    @ExceptionHandler(NoDataFoundException.class)
    public ResponseEntity<Map<String, String>> handleNoDataFoundException(
            NoDataFoundException ignoredNoDataFoundException) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Collections.singletonMap(MESSAGE, ignoredNoDataFoundException.getMessage()));
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Map<String, String>> handleConstraintViolationException(
            ConstraintViolationException ignoredConstraintViolationException) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ignoredConstraintViolationException.getMessage()));
    }

    @ExceptionHandler(InvalidUserRoleException.class)
    public ResponseEntity<Map<String, String>> handleInvalidUserRoleException(
            InvalidUserRoleException ignoredInvalidUserRoleException) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.INVALID_USER_ROLE.getMessage()));
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<Map<String, String>> handleUserNotFoundException(
            UserNotFoundException ignoredUserNotFoundException) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.USER_NOT_FOUND.getMessage()));
    }

    @ExceptionHandler(RestaurantNotFoundException.class)
    public ResponseEntity<Map<String, String>> handleRestaurantNotFoundException(
            RestaurantNotFoundException ignoredRestaurantNotFoundException) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.RESTAURANT_NOT_FOUND.getMessage()));
    }

    @ExceptionHandler(DishNotFoundException.class)
    public ResponseEntity<Map<String, String>> handleDishNotFoundException(
            DishNotFoundException ignoredDishNotFoundException) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.DISH_NOT_FOUND.getMessage()));
    }

    @ExceptionHandler(FieldNotUpdatableException.class)
    public ResponseEntity<Map<String, String>> handleFieldNotUpdatableException(
            FieldNotUpdatableException ignoredFieldNotUpdatableException) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ignoredFieldNotUpdatableException.getMessage()));
    }

    @ExceptionHandler(InvalidPropietarioException.class)
    public ResponseEntity<Map<String, String>> handleInvalidPropietarioException(
            InvalidPropietarioException ignoredInvalidPropietarioException) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN)
                .body(Collections.singletonMap(MESSAGE, ignoredInvalidPropietarioException.getMessage()));
    }

    @ExceptionHandler(InvalidDishException.class)
    public ResponseEntity<Map<String, String>> handleInvalidDishException(
            InvalidDishException ignoredInvalidDishException) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ignoredInvalidDishException.getMessage()));
    }

    @ExceptionHandler(InvalidEmployeeException.class)
    public ResponseEntity<Map<String, String>> handleInvalidEmployeeException(
            InvalidEmployeeException ignoredInvalidEmployeeException) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ignoredInvalidEmployeeException.getMessage()));
    }

    @ExceptionHandler(FeignException.class)
    public ResponseEntity<Map<String, String>> handleFeignException(
            FeignException ignoredFeignException) {
        return ResponseEntity.status(HttpStatus.MULTI_STATUS)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.FEIGN_EXCEPTION.getMessage()));
    }

    @ExceptionHandler(InvalidRequestException.class)
    public ResponseEntity<Map<String, String>> handleInvalidRequestExcepcion(
            InvalidRequestException ignoredInvalidRequestException) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ignoredInvalidRequestException.getMessage()));
    }
}