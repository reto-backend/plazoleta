package com.pragma.powerup.infrastructure.exceptionhandler;

public enum ExceptionResponse {
    NO_DATA_FOUND("No data found for the requested petition"),
    INVALID_USER_ROLE("Invalid user role for this action"),
    USER_NOT_FOUND("No user was found for the requested id"),
    RESTAURANT_NOT_FOUND("No restaurant was found for the requested id"),
    DISH_NOT_FOUND("No dish was found for the requested id"),
    FEIGN_EXCEPTION("Order ready, but there was an error while trying to send the SMS");

    private final String message;

    ExceptionResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}