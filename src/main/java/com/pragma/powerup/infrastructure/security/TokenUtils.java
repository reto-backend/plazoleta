package com.pragma.powerup.infrastructure.security;

import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import io.jsonwebtoken.Claims;

import java.security.Key;
import java.util.*;

public class TokenUtils {

    private TokenUtils() {
        throw new IllegalStateException("Utility class");
    }

    private static final String ACCESS_TOKEN_SECRET = "a8159ee1e46be62df0b8e047f5ff43adcb5c97172ffb6cefd0929f879d3a3fb5";

    public static UsernamePasswordAuthenticationToken getAuthentication(String token) {
        try {
            Claims claims = Jwts.parserBuilder()
                    .setSigningKey(getSigningKey())
                    .build()
                    .parseClaimsJws(token)
                    .getBody();

            String email = claims.getSubject();
            Collection<? extends GrantedAuthority> authorities = List.of(new SimpleGrantedAuthority(claims.get("role").toString()));
            return new UsernamePasswordAuthenticationToken(email, null, authorities);
        } catch (JwtException e) {
            return null;
        }
    }

    public static Long getUserId(String token) {
        token = cleanToken(token);
        try {
            Claims claims = Jwts.parserBuilder()
                    .setSigningKey(getSigningKey())
                    .build()
                    .parseClaimsJws(token)
                    .getBody();

            return Long.parseLong(claims.get("id").toString());
        } catch (JwtException e) {
            return null;
        }
    }

    private static String cleanToken(String token) {
        if (token.startsWith("Bearer ")) {
            return token.replace("Bearer ", "");
        }
        return token;
    }

    private static Key getSigningKey() {
        byte[] keyBytes = Decoders.BASE64.decode(ACCESS_TOKEN_SECRET);
        return Keys.hmacShaKeyFor(keyBytes);
    }
}
