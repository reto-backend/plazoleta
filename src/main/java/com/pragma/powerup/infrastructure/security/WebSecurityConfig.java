package com.pragma.powerup.infrastructure.security;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@RequiredArgsConstructor
@EnableWebSecurity
public class WebSecurityConfig {

    private final JwtAuthorizationFilter jwtAuthorizationFilter;

    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

        return http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/api/v1/restaurant/").hasRole("ADMINISTRADOR")
                .antMatchers(HttpMethod.POST,"/api/v1/dish/").hasRole("PROPIETARIO")
                .antMatchers(HttpMethod.PATCH,"/api/v1/dish/**").hasRole("PROPIETARIO")
                .antMatchers(HttpMethod.POST, "/api/v1/order/").hasRole("CLIENTE")
                .antMatchers(HttpMethod.GET, "/api/v1/order/**").hasRole("EMPLEADO")
                .antMatchers( "/api/v1/order/assign/**").hasRole("EMPLEADO")
                .antMatchers( "/api/v1/order/ready/**").hasRole("EMPLEADO")
                .antMatchers( "/api/v1/order/deliver/**").hasRole("EMPLEADO")
                .antMatchers( "/api/v1/order/cancel/**").hasRole("CLIENTE")
                .antMatchers(HttpMethod.GET,"/api/v1/dish/**").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/restaurant/").permitAll()
                .antMatchers("/swagger-ui.html",
                        "/swagger-ui/**",
                        "/swagger-resources/**",
                        "/swagger-resources",
                        "/v3/api-docs/**",
                        "/proxy/**",
                        "/api/v1/restaurant/{id}",
                        "/login").permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .addFilterBefore(jwtAuthorizationFilter, UsernamePasswordAuthenticationFilter.class)
                .build();
    }
}
