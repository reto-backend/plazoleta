package com.pragma.powerup.infrastructure.out.jpa.adapter;

import com.pragma.powerup.application.dto.request.SMSRequestDto;
import com.pragma.powerup.domain.model.DishQuantityModel;
import com.pragma.powerup.domain.model.OrderModel;
import com.pragma.powerup.domain.spi.IOrderPersistencePort;
import com.pragma.powerup.infrastructure.exception.InvalidDishException;
import com.pragma.powerup.infrastructure.exception.InvalidEmployeeException;
import com.pragma.powerup.infrastructure.exception.InvalidRequestException;
import com.pragma.powerup.infrastructure.exception.NoDataFoundException;
import com.pragma.powerup.infrastructure.out.jpa.entity.DishEntity;
import com.pragma.powerup.infrastructure.out.jpa.entity.DishQuantityEntity;
import com.pragma.powerup.infrastructure.out.jpa.entity.OrderEntity;
import com.pragma.powerup.infrastructure.out.jpa.entity.RestaurantEntity;
import com.pragma.powerup.infrastructure.out.jpa.feign.client.IMessageFeignClient;
import com.pragma.powerup.infrastructure.out.jpa.feign.client.IUserFeignClient;
import com.pragma.powerup.infrastructure.out.jpa.mapper.IDishQuantityEntityMapper;
import com.pragma.powerup.infrastructure.out.jpa.mapper.IOrderEntityMapper;
import com.pragma.powerup.infrastructure.out.jpa.repository.IDishQuantityRepository;
import com.pragma.powerup.infrastructure.out.jpa.repository.IDishRepository;
import com.pragma.powerup.infrastructure.out.jpa.repository.IOrderRepository;
import com.pragma.powerup.infrastructure.out.jpa.repository.IRestaurantRepository;
import com.pragma.powerup.infrastructure.security.TokenUtils;
import lombok.RequiredArgsConstructor;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@RequiredArgsConstructor
public class OrderJpaAdapter implements IOrderPersistencePort {
    private final IOrderRepository orderRepository;
    private final IOrderEntityMapper orderEntityMapper;

    private final IDishQuantityRepository dishQuantityRepository;
    private final IDishQuantityEntityMapper dishQuantityEntityMapper;

    private final IRestaurantRepository restaurantRepository;
    private final IDishRepository dishRepository;


    private final IUserFeignClient userFeignClient;
    private final IMessageFeignClient messageFeignClient;

    @Override
    public OrderModel saveOrder(OrderModel orderModel, Set<DishQuantityModel> dishQuantityModels, String token) {
        Optional<RestaurantEntity> restaurantEntity = restaurantRepository.findById(orderModel.getRestaurant().getId());
        if (restaurantEntity.isEmpty()) {
            throw new NoDataFoundException("No restaurant was found for the requested id");
        }

        validateDishes(dishQuantityModels, orderModel.getRestaurant().getId());
        validateClienteOrders(token);

        OrderEntity orderEntity = orderEntityMapper.toEntity(orderModel);
        Set<DishQuantityEntity> dishQuantityEntities= dishQuantityEntityMapper.toEntitySet(dishQuantityModels);

        orderEntity.setClienteId(TokenUtils.getUserId(token));

        orderEntity = orderRepository.save(orderEntity);
        for (DishQuantityEntity dishQuantityEntity : dishQuantityEntities) {

            dishQuantityEntity.setOrder(orderEntity);
            dishQuantityRepository.save(dishQuantityEntity);
        }

        orderEntity.setDishQuantities(dishQuantityEntities);
        return orderEntityMapper.toOrderModel(orderEntity);
    }

    @Override
    public OrderModel assignEmployee(Long orderId, String token) {
        Optional<OrderEntity> orderEntity = orderRepository.findById(orderId);
        if (orderEntity.isEmpty()) {
            throw new NoDataFoundException("No order was found for the requested id");
        }
        if (!Objects.equals(
                orderEntity.get().getRestaurant().getId(),
                userFeignClient.getEmpleado(TokenUtils.getUserId(token)).getRestaurantId())
        ){
            throw new InvalidEmployeeException("Employee with id " +
                    TokenUtils.getUserId(token) +
                    " does not belong to this restaurant");
        }
        orderEntity.get().setChefId(TokenUtils.getUserId(token));
        orderEntity.get().setStatus("en_preparacion");

        orderRepository.save(orderEntity.get());
        return orderEntityMapper.toOrderModel(orderEntity.get());
    }

    @Override
    public OrderModel orderReady(Long orderId, String token){
        Optional<OrderEntity> orderEntity = orderRepository.findById(orderId);
        if (orderEntity.isEmpty()) {
            throw new NoDataFoundException("No order was found for the requested id");
        }
        if(!Objects.equals(orderEntity.get().getChefId(), TokenUtils.getUserId(token))){
            throw new InvalidEmployeeException("Employee with id " +
                    TokenUtils.getUserId(token) +
                    " is not assigned to this order");
        }

        orderEntity.get().setStatus("listo");
        String pin = generatePin();
        orderEntity.get().setSecurityPin(pin);

        orderRepository.save(orderEntity.get());

        SMSRequestDto smsRequestDto = new SMSRequestDto();
        smsRequestDto.setDestinationSMSNumber(userFeignClient.getUser(orderEntity.get().getClienteId()).getCellphone());
        smsRequestDto.setSmsMessage("Tu orden está lista. Tu pin de seguridad es: " + pin);
        messageFeignClient.processSMS(smsRequestDto);

        return orderEntityMapper.toOrderModel(orderEntity.get());
    }

    @Override
    public void deliverOrder(Long orderId, String securityPin, String token){
        Optional<OrderEntity> orderEntity = orderRepository.findById(orderId);
        if (orderEntity.isEmpty()) {
            throw new NoDataFoundException("No order was found for the requested id");
        }
        if(!Objects.equals(orderEntity.get().getChefId(), TokenUtils.getUserId(token))){
            throw new InvalidEmployeeException("Employee with id " +
                    TokenUtils.getUserId(token) +
                    " is not assigned to this order");
        }
        if(!Objects.equals(orderEntity.get().getSecurityPin(), securityPin)){
            throw new InvalidRequestException("Invalid security pin");
        }
        if (!Objects.equals(orderEntity.get().getStatus(), "listo")){
            throw new InvalidRequestException("Order is not ready to be delivered");
        }

        orderEntity.get().setStatus("entregado");
        orderRepository.save(orderEntity.get());
    }

    @Override
    public void cancelOrder(Long orderId, String token){
        Optional<OrderEntity> orderEntity = orderRepository.findById(orderId);
        if (orderEntity.isEmpty()) {
            throw new NoDataFoundException("No order was found for the requested id");
        }
        if(!Objects.equals(orderEntity.get().getClienteId(), TokenUtils.getUserId(token))){
            throw new InvalidRequestException("Order with id " +
                    orderId +
                    " does not belong to this client");
        }
        if (!Objects.equals(orderEntity.get().getStatus(), "pendiente") &&
                !Objects.equals(orderEntity.get().getStatus(), "cancelado")){
            throw new InvalidRequestException("Lo sentimos, tu pedido ya esta en preparacion y no puede cancelarse");
        }

        orderEntity.get().setStatus("cancelado");
        orderRepository.save(orderEntity.get());
    }

    private void validateDishes(Set<DishQuantityModel> dishQuantityModels, Long restaurantId){
        Optional<DishEntity> dishEntity;
        for (DishQuantityModel dishQuantityModel : dishQuantityModels){
            dishEntity = dishRepository.findById(dishQuantityModel.getDish().getId());
            if (dishEntity.isEmpty()){
                throw new NoDataFoundException("Dish with id" +
                        dishQuantityModel.getDish().getId() +
                        "does not exist");
            }
            if (!Objects.equals(dishEntity.get().getRestaurant().getId(), restaurantId)){
                throw new InvalidDishException("Dish with id " +
                        dishQuantityModel.getDish().getId() +
                        " does not belong to this restaurant");
            }
        }
    }

    private void validateClienteOrders(String token){
        Long clienteId = TokenUtils.getUserId(token);
        for (OrderEntity orderEntity : orderRepository.findAllByClienteId(clienteId)){
            if (orderEntity.getStatus().equals("pendiente") ||
                    orderEntity.getStatus().equals("en_preparacion") ||
                    orderEntity.getStatus().equals("listo")){
                throw new InvalidRequestException("Cliente with id " +
                        clienteId +
                        " already has a pending order");
            }
        }
    }

    private String generatePin(){
        try{
            String chrs = "0123456789";
            SecureRandom secureRandom = SecureRandom.getInstanceStrong();

            return secureRandom
                    .ints(4, 0, chrs.length())
                    .mapToObj(chrs::charAt)
                    .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                    .toString();
        } catch (NoSuchAlgorithmException e){
            throw new NoDataFoundException("Error Generating Pin");
        }
    }
}
