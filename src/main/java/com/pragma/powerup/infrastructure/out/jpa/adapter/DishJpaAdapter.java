package com.pragma.powerup.infrastructure.out.jpa.adapter;

import com.pragma.powerup.domain.model.DishModel;
import com.pragma.powerup.domain.spi.IDishPersistencePort;
import com.pragma.powerup.infrastructure.exception.DishNotFoundException;
import com.pragma.powerup.infrastructure.exception.InvalidPropietarioException;
import com.pragma.powerup.infrastructure.exception.NoDataFoundException;
import com.pragma.powerup.infrastructure.exception.RestaurantNotFoundException;
import com.pragma.powerup.infrastructure.out.jpa.entity.CategoryEntity;
import com.pragma.powerup.infrastructure.out.jpa.entity.DishEntity;
import com.pragma.powerup.infrastructure.out.jpa.entity.RestaurantEntity;
import com.pragma.powerup.infrastructure.out.jpa.mapper.IDishEntityMapper;
import com.pragma.powerup.infrastructure.out.jpa.repository.ICategoryRepository;
import com.pragma.powerup.infrastructure.out.jpa.repository.IDishRepository;
import com.pragma.powerup.infrastructure.out.jpa.repository.IRestaurantRepository;
import com.pragma.powerup.infrastructure.security.TokenUtils;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
public class DishJpaAdapter implements IDishPersistencePort {
    private final IDishRepository dishRepository;
    private final IDishEntityMapper dishEntityMapper;
    private final IRestaurantRepository restaurantRepository;
    private final ICategoryRepository categoryRepository;

    @Override
    public DishModel saveDish(DishModel dishModel, String token) {
        Optional<RestaurantEntity> restaurantEntity = restaurantRepository.findById(dishModel.getRestaurant().getId());
        Optional<CategoryEntity> categoryEntity = categoryRepository.findById(dishModel.getCategory().getId());
        if (restaurantEntity.isEmpty()) {
            throw new RestaurantNotFoundException();
        }
        if (categoryEntity.isEmpty()) {
            throw new NoDataFoundException("No category was found for the requested id");
        }

        validatePropietario(token, restaurantEntity.get());

        DishEntity dishEntity = dishRepository.save(dishEntityMapper.toEntity(dishModel));
        return dishEntityMapper.toDishModel(dishEntity);
    }

    @Override
    public DishModel updateDish(DishModel dishModel, String token) {
        Optional<DishEntity> optionalDishEntity = dishRepository.findById(dishModel.getId());
        if (optionalDishEntity.isEmpty()) {
            throw new DishNotFoundException();
        }
        DishEntity dishEntity = optionalDishEntity.get();

        if (dishModel.getDescription() != null) {
            dishEntity.setDescription(dishModel.getDescription());
        }
        if (dishModel.getPrice() != null) {
            dishEntity.setPrice(dishModel.getPrice());
        }
        if (dishModel.getDescription() == null && dishModel.getPrice() == null) {
            dishEntity.setActive(dishModel.isActive());
        }

        validatePropietario(token, dishEntity.getRestaurant());

        dishRepository.save(dishEntity);
        return dishEntityMapper.toDishModel(dishEntity);
    }

    private void validatePropietario(String token, RestaurantEntity restaurantEntity) {
        Long claimedPropietarioId = TokenUtils.getUserId(token);
        if (!restaurantEntity.getPropietarioId().equals(claimedPropietarioId)) {
            throw new InvalidPropietarioException("You are not the Propietario of this restaurant");
        }
    }
}
