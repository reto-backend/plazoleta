package com.pragma.powerup.infrastructure.out.jpa.mapper;

import com.pragma.powerup.domain.model.DishQuantityModel;
import com.pragma.powerup.infrastructure.out.jpa.entity.DishQuantityEntity;
import org.mapstruct.Mapper;

import java.util.Set;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE,
        unmappedSourcePolicy = org.mapstruct.ReportingPolicy.IGNORE)
public interface IDishQuantityEntityMapper {
    DishQuantityEntity toEntity(DishQuantityModel dishQuantityModel);

    Set<DishQuantityEntity> toEntitySet(Set<DishQuantityModel> dishQuantityModels);
}
