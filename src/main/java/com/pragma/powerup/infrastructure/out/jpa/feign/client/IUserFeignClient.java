package com.pragma.powerup.infrastructure.out.jpa.feign.client;

import com.pragma.powerup.application.dto.response.EmpleadoResponseDto;
import com.pragma.powerup.application.dto.response.UserResponseDto;
import com.pragma.powerup.infrastructure.out.jpa.feign.configuration.FeignClientConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "user-service", url = "${users.ms.api.base-url}", configuration = FeignClientConfig.class)
public interface IUserFeignClient {

    @GetMapping(value = "/user/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    UserResponseDto getUser(@PathVariable("id") Long id);

    @GetMapping(value = "/empleado/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    EmpleadoResponseDto getEmpleado(@PathVariable("id") Long id);
}
