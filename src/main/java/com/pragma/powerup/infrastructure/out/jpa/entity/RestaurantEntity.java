package com.pragma.powerup.infrastructure.out.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "restaurants")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RestaurantEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    @NotBlank(message = "Name is required")
    @Pattern(regexp = "^.*\\D.*$", message = "Name cannot be all numbers")
    private String name;
    @NotBlank(message = "Address is required")
    private String address;
    @NotNull(message = "propietarioId is required")
    private Long propietarioId;
    @NotBlank(message = "Phone is required")
    @Pattern(regexp = "^\\+?\\d{1,12}$", message = "Phone is not valid")
    private String phone;
    @NotBlank(message = "urlLogo is required")
    private String urlLogo;
    @NotBlank(message = "nit is required")
    @Pattern(regexp = "^(\\d+)$", message = "NIT is not numeric")
    private String nit;
}
