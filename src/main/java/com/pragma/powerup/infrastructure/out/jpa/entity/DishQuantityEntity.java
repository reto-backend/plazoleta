package com.pragma.powerup.infrastructure.out.jpa.entity;

import com.pragma.powerup.infrastructure.out.jpa.key.DishQuantityKey;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "dish_quantity")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DishQuantityEntity {
    @EmbeddedId
    private DishQuantityKey id = new DishQuantityKey();
    @ManyToOne
    @MapsId("orderId")
    @JoinColumn(name = "order_id", referencedColumnName = "id")
    private OrderEntity order;
    @ManyToOne
    @MapsId("dishId")
    @JoinColumn(name = "dish_id", referencedColumnName = "id")
    private DishEntity dish;
    private Long quantity;
}
