package com.pragma.powerup.infrastructure.out.jpa.repository;

import com.pragma.powerup.infrastructure.out.jpa.entity.DishQuantityEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IDishQuantityRepository extends JpaRepository <DishQuantityEntity, Long>{
}
