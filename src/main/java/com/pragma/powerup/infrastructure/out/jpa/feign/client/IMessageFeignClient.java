package com.pragma.powerup.infrastructure.out.jpa.feign.client;

import com.pragma.powerup.application.dto.request.SMSRequestDto;
import com.pragma.powerup.infrastructure.out.jpa.feign.configuration.FeignClientConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

@FeignClient(name = "message-service", url = "${messages.ms.api.base-url}", configuration = FeignClientConfig.class)
public interface IMessageFeignClient {

    @PostMapping(value = "/processSMS", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Map<String, String>> processSMS(@RequestBody SMSRequestDto smsRequestDto);
}
