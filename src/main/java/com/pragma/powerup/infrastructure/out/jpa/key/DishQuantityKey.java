package com.pragma.powerup.infrastructure.out.jpa.key;

import lombok.Data;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
public class DishQuantityKey implements Serializable{

    private Long orderId;
    private Long dishId;
}
