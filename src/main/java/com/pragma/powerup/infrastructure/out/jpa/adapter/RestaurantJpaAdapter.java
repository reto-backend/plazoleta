package com.pragma.powerup.infrastructure.out.jpa.adapter;

import com.pragma.powerup.application.dto.response.UserResponseDto;
import com.pragma.powerup.domain.model.RestaurantModel;
import com.pragma.powerup.domain.shared.RoleEnum;
import com.pragma.powerup.domain.spi.IRestaurantPersistencePort;
import com.pragma.powerup.infrastructure.exception.InvalidUserRoleException;
import com.pragma.powerup.infrastructure.exception.NoDataFoundException;
import com.pragma.powerup.infrastructure.exception.UserNotFoundException;
import com.pragma.powerup.infrastructure.out.jpa.entity.RestaurantEntity;
import com.pragma.powerup.infrastructure.out.jpa.feign.client.IUserFeignClient;
import com.pragma.powerup.infrastructure.out.jpa.mapper.IRestaurantEntityMapper;
import com.pragma.powerup.infrastructure.out.jpa.repository.IRestaurantRepository;
import feign.FeignException;
import lombok.RequiredArgsConstructor;

import java.util.Objects;
import java.util.Optional;

@RequiredArgsConstructor
public class RestaurantJpaAdapter implements IRestaurantPersistencePort {

    private final IRestaurantRepository restaurantRepository;
    private final IRestaurantEntityMapper restaurantEntityMapper;
    private final IUserFeignClient userFeignClient;

    @Override
    public RestaurantModel saveRestaurant(RestaurantModel restaurantModel) {
        try {
            UserResponseDto userResponseDto = userFeignClient.getUser(restaurantModel.getPropietarioId());
            if (!Objects.equals(userResponseDto.getRole().getId(), RoleEnum.PROPIETARIO.getValue())){
                throw new InvalidUserRoleException();
            }

            RestaurantEntity restaurantEntity = restaurantRepository.save(restaurantEntityMapper.toEntity(restaurantModel));
            return restaurantEntityMapper.toRestaurantModel(restaurantEntity);
        } catch (FeignException e){
            throw new UserNotFoundException();
        }
    }

    @Override
    public RestaurantModel getRestaurant(Long id) {
        Optional<RestaurantEntity> restaurantEntity = restaurantRepository.findById(id);
        if (restaurantEntity.isEmpty()) {
            throw new NoDataFoundException("Restaurant with id " + id + " not found");
        }
        return restaurantEntityMapper.toRestaurantModel(restaurantEntity.get());
    }

}
