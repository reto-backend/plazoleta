package com.pragma.powerup.infrastructure.out.jpa.mapper;

import com.pragma.powerup.application.dto.response.DishResponseDto;
import com.pragma.powerup.domain.model.DishModel;
import com.pragma.powerup.infrastructure.out.jpa.entity.DishEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE,
        unmappedSourcePolicy = org.mapstruct.ReportingPolicy.IGNORE)
public interface IDishEntityMapper {
    DishEntity toEntity(DishModel dishModel);
    DishModel toDishModel(DishEntity dishEntity);
    DishResponseDto toResponse(DishEntity dishEntity);
}
