package com.pragma.powerup.infrastructure.input.rest;

import com.pragma.powerup.application.dto.request.DishRequestDto;
import com.pragma.powerup.application.dto.response.DishResponseDto;
import com.pragma.powerup.application.handler.IDishHandler;
import com.pragma.powerup.infrastructure.out.jpa.entity.CategoryEntity;
import com.pragma.powerup.infrastructure.out.jpa.mapper.IDishEntityMapper;
import com.pragma.powerup.infrastructure.out.jpa.repository.IDishRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/dish")
@RequiredArgsConstructor
public class DishRestController {
    private final IDishHandler dishHandler;
    private final IDishRepository dishRepository;
    private final IDishEntityMapper dishEntityMapper;

    @Operation(summary = "Create Dish")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Dish created", content = @Content),
            @ApiResponse(responseCode = "400", description = "Wrong Dish creation data", content = @Content),
            @ApiResponse(responseCode = "404", description = "Restaurant not found", content = @Content),
            @ApiResponse(responseCode = "404", description = "Category not found", content = @Content)
    })
    @PostMapping("/")
    public ResponseEntity<Void> saveDish(@RequestBody DishRequestDto dishRequestDto,
                                         @RequestHeader("Authorization") String token) {
        dishHandler.saveDish(dishRequestDto, token);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Operation(summary = "Update Dish")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Dish updated", content = @Content),
            @ApiResponse(responseCode = "400", description = "Wrong or invalid Dish update data", content = @Content),
            @ApiResponse(responseCode = "404", description = "Dish not found", content = @Content)
    })
    @PatchMapping("/{id}")
    public ResponseEntity<Void> updateDish(@PathVariable Long id,
                                           @RequestBody DishRequestDto dishRequestDto,
                                           @RequestHeader("Authorization") String token) {
        dishHandler.updateDish(dishRequestDto, id, token);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "Activate Dish")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Dish activated", content = @Content),
            @ApiResponse(responseCode = "404", description = "Dish not found", content = @Content)
    })
    @PatchMapping("/activate/{id}")
    public ResponseEntity<Void> activateDish(@PathVariable Long id,
                                           @RequestHeader("Authorization") String token) {
        dishHandler.activateDish(id, token);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "Deactivate Dish")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Dish deactivated", content = @Content),
            @ApiResponse(responseCode = "404", description = "Dish not found", content = @Content)
    })
    @PatchMapping("/deactivate/{id}")
    public ResponseEntity<Void> deactivateDish(@PathVariable Long id,
                                             @RequestHeader("Authorization") String token) {
        dishHandler.deactivateDish(id, token);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "List Dishes by Restaurant")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Dishes listed", content = @Content)
    })
    @GetMapping("/{restaurantId}")
    public Page<DishResponseDto> listDishesByRestaurant(
            @PathVariable Long restaurantId, Pageable pageable) {
        return dishRepository.findAllByRestaurantId(restaurantId, pageable)
                .map(dishEntityMapper::toResponse);
    }

    @Operation(summary = "List Dishes by Restaurant and Category")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Dishes listed", content = @Content)
    })
    @GetMapping("/{restaurantId}/{categoryId}")
    public Page<DishResponseDto> listDishesByRestaurantAndCategory(
            @PathVariable Long restaurantId,
            @PathVariable Long categoryId,
            Pageable pageable) {
        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setId(categoryId);
        return dishRepository.findAllByRestaurantIdAndCategory(restaurantId, categoryEntity, pageable)
                .map(dishEntityMapper::toResponse);
    }
}
