package com.pragma.powerup.infrastructure.input.rest;

import com.pragma.powerup.application.dto.request.OrderRequestDto;
import com.pragma.powerup.application.dto.response.OrderResponseDto;
import com.pragma.powerup.application.handler.IOrderHandler;
import com.pragma.powerup.infrastructure.out.jpa.feign.client.IUserFeignClient;
import com.pragma.powerup.infrastructure.out.jpa.mapper.IOrderEntityMapper;
import com.pragma.powerup.infrastructure.out.jpa.repository.IOrderRepository;
import com.pragma.powerup.infrastructure.security.TokenUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/order")
@RequiredArgsConstructor
public class OrderRestController {
    private final IOrderHandler orderHandler;
    private final IOrderRepository orderRepository;
    private final IOrderEntityMapper orderEntityMapper;

    private final IUserFeignClient userFeignClient;

    @Operation(summary = "Create Order")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Order created", content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid Order creation data", content = @Content),
            @ApiResponse(responseCode = "404", description = "Restaurant not found", content = @Content)
    })
    @PostMapping("/")
    public ResponseEntity<Void> saveOrder(@RequestBody OrderRequestDto orderRequestDto,
                                          @RequestHeader("Authorization") String token) {
        orderHandler.saveOrder(orderRequestDto, token);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Operation(summary = "List Orders by Status")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Orders listed", content = @Content)
    })
    @GetMapping("/{status}")
    public Page<OrderResponseDto> listOrdersByStatus(@PathVariable String status,
                                                     @RequestHeader("Authorization") String token,
                                                     Pageable pageable) {
        Long restaurantId = getRestaurantId(token);
        return orderRepository.findAllByRestaurantIdAndStatus(restaurantId, status, pageable)
                .map(orderEntityMapper::toResponse);
    }

    @Operation(summary = "Assign employee to order")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Employee assigned", content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid Order assign data", content = @Content),
            @ApiResponse(responseCode = "404", description = "Order not found", content = @Content)
    })
    @PatchMapping("/assign/{orderId}")
    public ResponseEntity<Void> assignEmployee(@PathVariable Long orderId,
                                                @RequestHeader("Authorization") String token) {
        orderHandler.assignEmployee(orderId, token);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "Order Ready")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Order ready", content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid Order ready data", content = @Content),
            @ApiResponse(responseCode = "404", description = "Order not found", content = @Content),
            @ApiResponse(responseCode = "207", description = "Order ready, error sending SMS", content = @Content)
    })
    @PatchMapping("/ready/{orderId}")
    public ResponseEntity<Void> orderReady(@PathVariable Long orderId,
                                            @RequestHeader("Authorization") String token) {
        orderHandler.orderReady(orderId, token);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "Deliver Order")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Order delivered", content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid Order deliver data", content = @Content),
            @ApiResponse(responseCode = "404", description = "Order not found", content = @Content)
    })
    @PatchMapping("/deliver/{orderId}/{securityPin}")
    public ResponseEntity<Void> deliverOrder(@PathVariable Long orderId,
                                              @PathVariable String securityPin,
                                              @RequestHeader("Authorization") String token) {
        orderHandler.deliverOrder(orderId, securityPin, token);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "Cancel Order")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Order canceled", content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid Order cancel data", content = @Content),
            @ApiResponse(responseCode = "404", description = "Order not found", content = @Content)
    })
    @PatchMapping("/cancel/{orderId}")
    public ResponseEntity<Void> cancelOrder(@PathVariable Long orderId,
                                              @RequestHeader("Authorization") String token) {
        orderHandler.cancelOrder(orderId, token);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private Long getRestaurantId(String token) {
        Long empleadoId = TokenUtils.getUserId(token);
        return userFeignClient.getEmpleado(empleadoId).getRestaurantId();
    }

}
