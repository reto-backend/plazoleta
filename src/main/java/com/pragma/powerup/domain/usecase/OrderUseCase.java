package com.pragma.powerup.domain.usecase;

import com.pragma.powerup.domain.api.IOrderServicePort;
import com.pragma.powerup.domain.model.DishQuantityModel;
import com.pragma.powerup.domain.model.OrderModel;
import com.pragma.powerup.domain.spi.IOrderPersistencePort;

import java.util.Set;

public class OrderUseCase implements IOrderServicePort {
    private final IOrderPersistencePort orderPersistencePort;

    public OrderUseCase(IOrderPersistencePort orderPersistencePort) {
        this.orderPersistencePort = orderPersistencePort;
    }
    @Override
    public void saveOrder(OrderModel orderModel, Set<DishQuantityModel> dishQuantityModels, String token) {
        orderPersistencePort.saveOrder(orderModel, dishQuantityModels, token);
    }

    @Override
    public void assignEmployee(Long orderId, String token) {
        orderPersistencePort.assignEmployee(orderId, token);
    }

    @Override
    public void orderReady(Long orderId, String token) {
        orderPersistencePort.orderReady(orderId, token);
    }

    @Override
    public void deliverOrder(Long orderId, String securityPin, String token) {
        orderPersistencePort.deliverOrder(orderId, securityPin, token);
    }

    @Override
    public void cancelOrder(Long orderId, String token) {
        orderPersistencePort.cancelOrder(orderId, token);
    }
}
