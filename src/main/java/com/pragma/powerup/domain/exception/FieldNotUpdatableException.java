package com.pragma.powerup.domain.exception;

public class FieldNotUpdatableException extends RuntimeException{
    public FieldNotUpdatableException(String message) {
        super(message);
    }
}
