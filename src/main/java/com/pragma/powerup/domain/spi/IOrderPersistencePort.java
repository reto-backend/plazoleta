package com.pragma.powerup.domain.spi;

import com.pragma.powerup.domain.model.DishQuantityModel;
import com.pragma.powerup.domain.model.OrderModel;

import java.util.Set;

public interface IOrderPersistencePort {

    OrderModel saveOrder(OrderModel orderModel, Set<DishQuantityModel> dishQuantityModels, String token);

    OrderModel assignEmployee(Long orderId, String token);

    OrderModel orderReady(Long orderId, String token);

    void deliverOrder(Long orderId, String securityPin, String token);

    void cancelOrder(Long orderId, String token);
}
