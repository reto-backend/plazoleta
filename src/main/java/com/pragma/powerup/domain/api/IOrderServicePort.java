package com.pragma.powerup.domain.api;

import com.pragma.powerup.domain.model.DishQuantityModel;
import com.pragma.powerup.domain.model.OrderModel;

import java.util.Set;

public interface IOrderServicePort {
    void saveOrder(OrderModel orderModel, Set<DishQuantityModel> dishQuantityModels, String token);

    void assignEmployee(Long orderId, String token);

    void orderReady(Long orderId, String token);

    void deliverOrder(Long orderId, String securityPin, String token);

    void cancelOrder(Long orderId, String token);
}
