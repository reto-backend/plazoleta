package com.pragma.powerup.domain.model;

public class DishQuantityModel {
    private OrderModel order;
    private DishModel dish;
    private Long quantity;

    public DishQuantityModel() {
    }

    public DishQuantityModel(OrderModel order, DishModel dish, Long quantity) {
        this.order = order;
        this.dish = dish;
        this.quantity = quantity;
    }

    public OrderModel getOrder() {
        return order;
    }

    public void setOrder(OrderModel order) {
        this.order = order;
    }

    public DishModel getDish() {
        return dish;
    }

    public void setDish(DishModel dish) {
        this.dish = dish;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
