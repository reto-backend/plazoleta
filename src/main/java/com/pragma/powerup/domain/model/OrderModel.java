package com.pragma.powerup.domain.model;

import java.util.Date;

public class OrderModel {
    private Long id;
    private Long clienteId;
    private Date date;
    private String status = "pendiente";
    private Long chefId;
    private String securityPin;
    private RestaurantModel restaurant;

    public OrderModel() {
    }

    public OrderModel(Long id, Long clienteId, Date date, String status, Long chefId, String securityPin, RestaurantModel restaurant) {
        this.id = id;
        this.clienteId = clienteId;
        this.date = date;
        this.status = status;
        this.chefId = chefId;
        this.securityPin = securityPin;
        this.restaurant = restaurant;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId) {
        this.clienteId = clienteId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getChefId() {
        return chefId;
    }

    public void setChefId(Long chefId) {
        this.chefId = chefId;
    }

    public String getSecurityPin() {
        return securityPin;
    }

    public void setSecurityPin(String securityPin) {
        this.securityPin = securityPin;
    }

    public RestaurantModel getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(RestaurantModel restaurant) {
        this.restaurant = restaurant;
    }
}
